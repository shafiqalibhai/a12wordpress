=== Advanced Search by Axis12 ===
Contributors: www.axistwelve.com
Author URI: http://www.axistwelve.com
Tags: solr, search, search results, search integration, custom search, better search, search replacement, category search, comment search, tag search, page search, post search, search highlight, seo
Requires at least: 3.0.0
Tested up to: 3.8.1
Stable tag: 2.0.5


A WordPress plugin that replaces the default WordPress search with a lot of benefits


== Description ==

Advanced Search by Axis12 plugin replaces the default WordPress search.
Features and benefits include:

*   Index pages, posts and custom post types
*   Enable search and faceting on fields such as tags, categories, author, page type, custom fields and custom taxonomies
*   Add special template tags so you can create your own custom result pages to match your theme.
*   Search term suggestions (AutoComplete)
*   Provides better search results based on relevancy
*   Create custom summaries with the search terms highlighted
*   Completely integrated into default WordPress theme and search widget.
*   Configuration options allow you to select pages to ignore

== Installation ==

= Prerequisite =

In order to have spell checking work, in your solrconfig.xml file, check :

1. the spellchecker component have to be correctly configured :

    &lt;lst name="spellchecker"&gt;
      &lt;str name="name">default&lt;/str&gt;
      &lt;str name="field">spell&lt;/str&gt;
      &lt;str name="spellcheckIndexDir"&gt;spellchecker&lt;/str&gt;
      &lt;str name="buildOnOptimize"&gt;true&lt;/str&gt;
    &lt;/lst&gt;
   
2. the request handler includes the spellchecker component

     &lt;arr name="last-components"&gt;
       &lt;str&gt;spellcheck&lt;/str&gt;
     &lt;/arr&gt;

= Installation =

1. Upload the `enterprise-search` folder to the `/wp-content/plugins/`
directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go in Advanced Search by My Solr Server settings page ("Advanced Search by Axis12"), configure the plugin and Load your blog content in Solr ("Load Content" button)

= Customize the plugin display =

The plugin uses the template files located in the `template` directory. You can implement your own template files by copying theses files with a new name terminating by "_custom" (for instance, the file mss_search.php is copied as mss_search_custom.php). These new files can be located in the plugin's template directory or in your theme's main directory.


== Frequently Asked Questions ==

= What version of WordPress does Advanced Search by Axis12 plugin work with? =

Advanced Search by Axis12 plugin works with WordPress 3.0.0 and greater.

= How to manage Custom Post type, custom taxonomies and custom fields? =

Advanced Search by Axis12 plugin was tested with:
* "Custom Post Type UI" plugin for Custom Post type and custom taxonomies management 
* "Custom Field Template" plugin for custom fields management
* WP-Types plugin for Custom Post type and custom taxonomies management and for custom fields management

== Screenshots ==

1. Configuration page


2. Configuration page (facets)